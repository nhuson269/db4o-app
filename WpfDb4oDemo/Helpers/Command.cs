﻿using System;
using System.Windows.Input;

namespace WpfDb4oDemo.Helpers
{
    public class Command : ICommand
    {
        Action _action;
        bool _canExecute;

        public event EventHandler CanExecuteChanged;

        public Command(Action execute, bool canExecute = true)
        {
            _action = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute;
        }

        public void Execute(object parameter)
        {
            _action();
        }      
    }
}
