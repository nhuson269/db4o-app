﻿using WpfDb4oDemo.Helpers;

namespace WpfDb4oDemo.Models
{
    public class Item
    {
        public string MaSv { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }

        public Item(string _maSv, string _fullName, string _gender)
        {
            this.MaSv = _maSv;
            this.FullName = _fullName;
            this.Gender = _gender;
        }

        public override string ToString()
        {
            return MaSv + " - " + FullName + " - " + Gender;
        }
    }
}
