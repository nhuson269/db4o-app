﻿using Db4objects.Db4o;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using WpfDb4oDemo.Helpers;
using WpfDb4oDemo.Models;

namespace WpfDb4oDemo.ViewModels
{
    public class MainViewModel : Observable
    {
        IObjectContainer db;

        string _maSv;       
        public string MaSv
        {
            get
            {
                return _maSv;
            }
            set
            {
                if (value != _maSv)
                {
                    _maSv = value;
                    OnPropertyChaned("MaSv");
                }
            }
        }

        string _fullName;
      
        public string FullName
        {
            get
            {
                return _fullName;
            }
            set
            {
                if (value != _fullName)
                {
                    _fullName = value;
                    OnPropertyChaned("FullName");
                }
            }
        }

        string _gender = "Unknown";

        public string Gender
        {
            get
            {
                return _gender;
            }
            set
            {
                if (value != _gender)
                {
                    _gender = value;
                    OnPropertyChaned("Gender");
                }
            }
        }

        string _maSvKey;
        public string MaSvKey
        {
            get
            {
                return _maSvKey;
            }
            set
            {
                if (value != _maSvKey)
                {
                    _maSvKey = value;
                    OnPropertyChaned("MaSvKey");
                }
            }
        }

        ICommand _loadAllCommand;
        public ICommand LoadAllCommand
        {
            get
            {
                return _loadAllCommand ?? (_loadAllCommand = new Command(() => LoadData()));
            }
        }

        ICommand _addCommand;
        public ICommand AddCommand
        {
            get
            {
                return _addCommand ?? (_addCommand = new Command(() => Add()));
            }
        }

        ICommand _searchCommand;
        public ICommand SearchCommand
        {
            get
            {
                return _searchCommand ?? (_searchCommand = new Command(() => Search()));
            }
        }

        ICommand _deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _deleteCommand ?? (_deleteCommand = new Command(() => Delete()));
            }
        }

        ICommand _deleteAllCommand;
        public ICommand DeleteAllCommand
        {
            get
            {
                return _deleteAllCommand ?? (_deleteAllCommand = new Command(() => DeleteAll()));
            }
        }

        ObservableCollection<Item> _items = new ObservableCollection<Item>();

        public ObservableCollection<Item> Items
        {
            get
            {
                return _items;
            }
            set
            {
                if (value != _items)
                {
                    _items = value;
                    OnPropertyChaned("Items");
                }
            }
        }

        public MainViewModel()
        {
            db = Db4oEmbedded.OpenFile(Db4oEmbedded.NewConfiguration(), "FileData.odb4");           
        }

        public void LoadData()
        {
            try
            {
                Items.Clear();
                Item item = new Item(null, null, null);
                IObjectSet result = db.QueryByExample(item);
                foreach (Item it in result)
                {
                    Items.Add(it);
                }
            }
            catch (Exception ex)
            {
                db.Close();
                Debug.WriteLine("Error: " + ex.Message);
            }
        }

        public void Close()
        {
            db.Close();
        }

        void Add()
        {
            if (!string.IsNullOrEmpty(MaSv) && !string.IsNullOrEmpty(FullName) && (MaSv.Length > 0) && (FullName.Length > 0))
            {
                try
                {
                    Item item = new Item(MaSv, FullName, Gender);
                    db.Store(item);
                    Items.Add(item);
                    MaSv = FullName = null;
                    Debug.WriteLine("Stored: " + item);
                }
                catch (Exception ex)
                {
                    db.Close();
                    Debug.WriteLine("Error: " + ex.Message);
                }
            }
        }

        void Search()
        {        
            if (!string.IsNullOrEmpty(MaSvKey) && (MaSvKey.Length > 0))
            {
                try
                {
                    Items.Clear();                  
                    IObjectSet result = db.QueryByExample(new Item(MaSvKey, null, null));
                    foreach (Item it in result)
                    {
                        Items.Add(it);
                    }
                    MaSvKey = null;
                    MessageBox.Show("Hãy ấn Clean All trước khi làm nhiệm vụ khác");
                }
                catch (Exception ex)
                {
                    db.Close();
                    Debug.WriteLine("Error: " + ex.Message);
                }
            }
        }

        void Delete()
        {
            if (!string.IsNullOrEmpty(MaSvKey) && (MaSvKey.Length > 0))
            {
                try
                {                 
                    IObjectSet result = db.QueryByExample(new Item(MaSvKey, null, null));
                    foreach (Item it in result)
                    {
                        db.Delete(it);
                        Items.Remove(it);
                    }
                    MaSvKey = null;
                    MessageBox.Show("Hãy ấn Clean All trước khi làm nhiệm vụ khác");
                }
                catch (Exception ex)
                {
                    db.Close();
                    Debug.WriteLine("Error: " + ex.Message);
                }
            }
        }

        void DeleteAll()
        {
            try
            {
                IObjectSet result = db.QueryByExample(new Item(null, null, null));
                foreach (Item it in result)
                {
                    db.Delete(it);
                    Items.Remove(it);
                }            
            }
            catch (Exception ex)
            {
                db.Close();
                Debug.WriteLine("Error: " + ex.Message);
            }
        }
    }
}
