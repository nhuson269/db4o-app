﻿using System;
using System.Windows;
using WpfDb4oDemo.ViewModels;

namespace WpfDb4oDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainViewModel MainData = new MainViewModel();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = MainData;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            MainData.LoadData();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            MainData.Close();
        }
    }
}
